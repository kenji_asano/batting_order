import os
import pandas as pd
import numpy as np
import random

dirname = 'batting_average_data'

class Player:
    def __init__(self, name, ave_0out_csv, ave_1out_csv, ave_2out_csv):
        self.name = name
        self.ave = [pd.read_csv(os.path.join(dirname, ave_0out_csv), index_col=0), \
                        pd.read_csv(os.path.join(dirname, ave_1out_csv), index_col=0), \
                        pd.read_csv(os.path.join(dirname, ave_2out_csv), index_col=0)]

    def batting(self, out, runner, verbose):
        action = self.get_action(out, runner)
        if verbose:
            print(f'player: {self.name} action: {action}')

        return self.get_result(out, runner, action)

    def get_action(self, out, runner):
        table = self.ave[out]
        average = table[str(runner)]
        action = np.random.choice(list(average.index), p=list(average))

        return action

    def get_result(self, out, runner, action):
        # ランナーの移動はビット演算で表現する。3つのbitで、それぞれの塁にランナーがいたら1がたつ。
        # ヒットの場合は左に1シフト(あるいは2シフト)して1塁ビットに1をたてる。
        # 2塁打の場合には左に2シフト(あるいは3シフト)して2塁ビットに1をたてる。
        # 3塁打の場合には左に3シフトして3塁ビットに1をたてる。
        # ホームランの場合には左に4シフトして第4ビットに1をたてる。
        # 上記操作後に第4ビット以上に1がたっている数が得点。
        # 犠打の場合は左に1シフトしてアウトを1増やす。
        # アウトの場合はアウトを1増やす。

        if action == 'out':
            out += 1
        elif action == 'sacrifice':
            runner = runner << 1
            out += 1
        elif action == 'single':
            if random.random() > 0.5:
                runner = runner << 1
            else:
                runner = runner << 2
            runner = runner | 0b0001
        elif action == 'double':
            if random.random() > 0.5:
                runner = runner << 2
            else:
                runner = runner << 3
            runner = runner | 0b0010
        elif action == 'tripple':
            runner = runner << 3
            runner = runner | 0b0100
        elif action == 'homerun':
            runner = runner << 4
            runner = runner | 0b1000

        run, runner = self.get_score(runner)

        return out, runner, run

    def get_score(self, runner):
        run = bin(runner & 0b1111000).count('1')
        runner = runner & 0b111

        return run, runner

class Team:
    def __init__(self, player_list, order, verbose=False):
        self.player_list = player_list
        self.order = order
        self.n_inning = 9
        self.score = 0
        self.next_batter_id = 0
        self.verbose = verbose

    def inning(self):
        out = 0
        runner = 0

        while out < 3:
            player = self.player_list[self.order.index(self.next_batter_id)]
            out, runner, run = player.batting(out, runner, self.verbose)
            self.score += run
            self.next_batter_id = (self.next_batter_id + 1) % 9

    def play(self):
        for i in range(self.n_inning):
            self.inning()
            if self.verbose:
                print(f'inning: {i+1} score:{self.score}\n')

        return self.score

    def reset(self):
        self.score = 0
        self.next_batter_id = 0

Leadoff = Player('Leadoff', 'Leadoff - 0.csv', 'Leadoff - 1.csv', 'Leadoff - 2.csv')
Kawai = Player('Kawai', 'Kawai - 0.csv', 'Kawai - 1.csv', 'Kawai - 2.csv')
Ichiro = Player('Ichiro', 'Ichiro - 0.csv', 'Ichiro - 1.csv', 'Ichiro - 2.csv')
Bass = Player('Bass', 'Bass - 0.csv', 'Bass - 1.csv', 'Bass - 2.csv')
Komada = Player('Komada', 'Komada - 0.csv', 'Komada - 1.csv', 'Komada - 2.csv')
Tatsunami = Player('Tatsunami', 'Tatsunami - 0.csv', 'Tatsunami - 1.csv', 'Tatsunami - 2.csv')
Lance = Player('Lance', 'Lance - 0.csv', 'Lance - 1.csv', 'Lance - 2.csv')
Pitcher = Player('Pitcher', 'Pitcher - 0.csv', 'Pitcher - 1.csv', 'Pitcher - 2.csv')
Joker = Player('Joker', 'Joker - 0.csv', 'Joker - 1.csv', 'Joker - 2.csv')

player_list = [Leadoff, Kawai, Ichiro, Bass, Komada, Tatsunami, Lance, Pitcher, Joker]

import itertools
from tqdm import tqdm

n_games = 100
base_order = [i for i in range(9)]

record = pd.DataFrame()

for order_id, order in enumerate(tqdm(itertools.permutations(base_order))):
    scores = []
    for i in range(n_games):
        team = Team(player_list, order, verbose=False)
        score = team.play()
        scores.append(score)

    record[str(order_id)] = scores
    record[str(order_id)] = record[str(order_id)].astype('int8')

record.to_csv('record.csv')
